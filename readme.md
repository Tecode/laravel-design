## 毕业设计项目《景区售票系统》
##### 开发者：漆艳红
## 安装
```bash
git clone https://gitlab.com/Tecode/laravel-design.git
composer install
```
## 环境要求
- PHP >= 5.6.4
- OpenSSL PHP Extension
- PDO PHP Extension
- Mbstring PHP Extension
- Tokenizer PHP Extension
- XML PHP Extension
## 说明
- public/front文件夹放的是前端页面文件资源
- resources/views/front文件夹放置前端页面模板
- 本地访问：http://127.0.0.1:1009
- 线上Demo：http://47.98.212.129:1009/